package triangle

object Triangle {

  def determine(a: Int, b: Int, c: Int) : String = {
    var result = "Error"
    if (a > 0 && b > 0 && c > 0 && (a+b) > c && (a+c) > b && (b+c) > a)
      if (a == b && a == c) result = "Triangle is equilateral"
      else if (a == b || a == c || b == c) result = "Triangle is isosceles"
      else result = "Triangle is scalene"
    println(result)
    return result
  }
  
  def main(args: Array[String]) {
    println("Input first edge:")
    val a = Console.readInt
    println("Input secord edge:")
    val b = Console.readInt
    println("Input third edge:")
    val c = Console.readInt
    determine(a, b, c)
  }
}
