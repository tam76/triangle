package triangle

import org.scalatest.FunSuite

class TriangleTest extends FunSuite {

  test("equilateral") {
    println("Test triangle type with 3 egdes are 1, 1, 1")
    assert(Triangle.determine(1, 1, 1) === "Triangle is equilateral")
  }

  test("isosceles") {
    println("Test triangle type with 3 egdes are 2, 2, 3")
    assert(Triangle.determine(2,2,3) === "Triangle is isosceles")
  }

  test("scalence") {
    println("Test triangle type with 3 egdes are 2, 3, 4")
    assert(Triangle.determine(2,3,4) === "Triangle is scalene")
  }

  test("error") {
    println("Test triangle type with 3 egdes are 1, 2, 3")
    assert(Triangle.determine(1,2,3) === "Error")

    println("Test triangle type with 3 egdes are -1, 1, 1")
    assert(Triangle.determine(-1,1,1) === "Error")

    println("Test triangle type with 3 egdes are 1, -1, 1")
    assert(Triangle.determine(1,-1,1) === "Error")

    println("Test triangle type with 3 egdes are 1, 1, -1")
    assert(Triangle.determine(1,1,-1) === "Error")
  }
}
